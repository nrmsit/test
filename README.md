# 🥞 Pancake Frontend

[![Netlify Status](https://api.netlify.com/api/v1/badges/7bebf1a3-be7b-4165-afd1-446256acd5e3/deploy-status)](https://app.netlify.com/sites/pancake-prod/deploys)

This project contains the main features of the pancake application.

If you want to contribute, please refer to the [contributing guidelines](./CONTRIBUTING.md) of this project.


# docker login registry.gitlab.com

# docker build -t registry.gitlab.com/aquafinance.octo/web_register .

# docker push registry.gitlab.com/aquafinance.octo/web_register