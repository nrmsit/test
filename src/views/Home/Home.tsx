import React, { useState } from 'react'
import styled from 'styled-components'
import { Heading, Text, BaseLayout } from '@pancakeswap/uikit'
import { useWeb3React } from '@web3-react/core'
import useAuth from 'hooks/useAuth'
import Page from 'components/layout/Page'
import UserBlock from 'components/UserBlock'
import axios from 'axios'
import '../../style/style.css'




const Home: React.FC = () => {
  const { account } = useWeb3React()
  const { login, logout } = useAuth()

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [address, setAddress] = useState("");
  const [telephone, setPhoneNumber] = useState("");
  const [sizeShirt, setSizeShirt] = useState("");

  const [myText, setMyText] = useState(" ");
  const [showButton, setShowButton] = useState(true);
  const [showForm, setShowForm] = useState(false);
  const [textAirdrop, setTextAirDrop] = useState("");
  const [sentenceAirdrop, setSentenceAirdrop] = useState("");
  const [textPrize, setTextPrize] = useState("");



  console.log('Home.tsx account :', account)

  const textResultCheckList = document.getElementById("textResultCheckList")
  const textCongrat = document.getElementById("congrat")
  const textSentence = document.getElementById("sentence")
  const textResultPrize = document.getElementById("textResultPrize")

  const message = () => {
    const url = 'http://35.198.247.48:3000/checkList/' + account

    console.log("url", url)

    if(account === 'undefined'){
      console.log('Please connect Wallet')
    }else{
      console.log('Button Account :', account)
      fetch(url).then((response) => response.json())
          .then(function(data) { /* do stuff with your JSON data */
            console.log('data', data)
            console.log('data length', data.length)
            if(data.length === 0 ){
              console.log('array null' , data)
              // textResultCheckList.innerHTML = "ข้อมูลที่ตรวจสอบได้ : คุณไม่ได้รับรางวัล"
              textCongrat.innerHTML = "Sorry"
              textSentence.innerHTML = "You haven't got an airdrop."
            }else{
              console.log('array not null' , data)
              // textResultCheckList.innerHTML = "ข้อมูลที่ตรวจสอบได้ : คุณได้รับรางวัล"
              textCongrat.innerHTML = "Congrats!!"
              textSentence.innerHTML = "You've got an airdrop."
              if(data[0].AQUA_BAG && data[0].AQUA_Shirt === true){
                // textResultPrize.innerHTML = "ของรางวัลที่ได้รับ : เสื้อ และ กระเป๋า"
                setTextPrize("AQUA T-Shirt & AQUA Bag")
              }else if (data[0].AQUA_BAG === true){
                // textResultPrize.innerHTML = "ของรางวัลที่ได้รับ : กระเป๋า"
                setTextPrize("AQUA Bag")
              }else if(data[0].AQUA_Shirt === true){
                // textResultPrize.innerHTML = "ของรางวัลที่ได้รับ : เสื้อ"
                setTextPrize("AQUA T-Shirt")
              }

              setShowButton(false)
              setShowForm(true)
            }
          })
          .catch((error) => console.log(error))
    }
   }

/*    const dataMock = {
    walletAddress : "0x2b3db1ea42bf8adc8708d7e62d145d2d6a998711",
    firstname: "sssssss",
    lastname: "ffffff",
    address: "gggggggg",
    telephone: "0900000000",
    size :" "
}; */

   const urlAddData = "http://35.198.247.48:3000/addData"

   const mySubmitHandler = (event) => {
    event.preventDefault();
    const getFirstname = event.target.firstName.value
    const getLastname = event.target.lastName.value
    const getAddress = event.target.address.value
    const getTelephone = event.target.telephone.value
    const getSizeShirt = event.target.sizeShirt.value
    axios
    .post(urlAddData, {
      walletAddress : account,
      firstname: getFirstname,
      lastname: getLastname,
      address: getAddress,
      telephone: getTelephone,
      size : getSizeShirt,
      prize: textPrize

  })
    .then(response => {
      console.log("response: ", response)
        // do something about response
      })
    .catch(err => {
      console.error(err)
    })
    alert("You're Submitted")

  }
  
  const clickForShowForm = () => {
    setShowButton(false)
    setShowForm(true)
  }


  return (
    <Page>
      <div className="connect-container">
        <UserBlock account={account} login={login} logout={logout} />
      </div> <br />
      <div className="title-container">Airdrop Check-list</div>
      <div className="img-container">
        <img src="Aqua Seal Charactor-17 1.png" alt="" />
      </div><br />

      {/* <button type="button" 
      style={{visibility: showButton ? 'visible' : 'hidden' }}
      onClick={() => clickForShowForm()}>
        Show Form
      </button> */}



      <div className="content-container">
        
      <button type="button" className="button-check"
      style={{visibility: showButton ? 'visible' : 'hidden' }}
      onClick={() => message()}>
        Click for Check
      </button>

      <div >
        <div className="result-container">
          <div className="text1" id="congrat">{textAirdrop}</div>
          <div className="text2" id="sentence">{sentenceAirdrop}</div>
        </div>        
        <div className="form-container" style={{visibility: showForm ? 'visible' : 'hidden' }}>
          <div className="form-title">Please fill in delivery form</div>
          <form  onSubmit={mySubmitHandler}>
            <p className="labelGift">Premium Gift: {textPrize} </p>
            <p className="label">Wallet Address</p>
            <p className="label2">{account}</p>
            <p className="label">Name</p>
            <input className="label2"
              value={firstName}
              onChange={e => setFirstName(e.target.value)}
              placeholder="Name"
              type="text"
              name="firstName"
              required
            />
            <p className="label">Lastname</p>
            <input  className="label2"
              value={lastName}
              onChange={e => setLastName(e.target.value)}
              placeholder="Lastname"
              type="text"
              name="lastName"
              required
            />

            <p className="label">
              Address
            </p>
            <input  className="label2"
              value={address}
              onChange={e => setAddress(e.target.value)}
              placeholder="Address"
              type="text"
              name="address"
              required
            />
            <p  className="label">
              Tel
            </p>
              <input  className="label2"
              value={telephone}
              onChange={e => setPhoneNumber(e.target.value)}
              placeholder="0XXXXXXXXX"
              type="tel"
              pattern="[0-9]{10}"
              name="telephone"
              required
            /> 

            <p className="label">
              T-Shirt size (If you&#39;ve got T-shirt)
            </p>
            <input className="label2" 
              value={sizeShirt}
              onChange={e => setSizeShirt(e.target.value)}
              placeholder="T-Shirt size (If you&#39;ve got T-shirt)"
              type="text"
              name="sizeShirt"
            />

            <button className="form-button" type="submit">Submit</button>
            <br /> <br />
            <br /> <br />
          </form>
        </div>
      </div>

        
     </div> 
    </Page>
  )
}

export default Home
