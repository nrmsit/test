import { ContextApi } from 'contexts/Localization/types'
import { PageMeta } from './types'

export const DEFAULT_META: PageMeta = {
  title: 'AQUAFinance',
  description:
    'GRAB YOUR OXYGEN TANK - WE ARE HEADING TO THE MOON',
}

export const getCustomMeta = (path: string, t: ContextApi['t']): PageMeta => {
  switch (path) {
    case '/':
      return {
        title: `${t('Home')} | ${t('AQUAFinance')}`,
      }
    case '/competition':
      return {
        title: `${t('Trading Battle')} | ${t('AQUAFinance')}`,
      }
    case '/prediction':
      return {
        title: `${t('Prediction')} | ${t('AQUAFinance')}`,
      }
    case '/farms':
      return {
        title: `${t('Farms')} | ${t('AQUAFinance')}`,
      }
    case '/pools':
      return {
        title: `${t('Pools')} | ${t('AQUAFinance')}`,
      }
    case '/lottery':
      return {
        title: `${t('Lottery')} | ${t('AQUAFinance')}`,
      }
    case '/collectibles':
      return {
        title: `${t('Collectibles')} | ${t('AQUAFinance')}`,
      }
    case '/ifo':
      return {
        title: `${t('Initial Farm Offering')} | ${t('AQUAFinance')}`,
      }
    case '/teams':
      return {
        title: `${t('Leaderboard')} | ${t('AQUAFinance')}`,
      }
    case '/profile/tasks':
      return {
        title: `${t('Task Center')} | ${t('AQUAFinance')}`,
      }
    case '/profile':
      return {
        title: `${t('Your Profile')} | ${t('AQUAFinance')}`,
      }
    default:
      return null
  }
}
