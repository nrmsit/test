import React, { useState } from 'react'
import { ThemeProvider as SCThemeProvider } from 'styled-components'
import { light, dark } from '@pancakeswap/uikit'

const CACHE_KEY = 'IS_DARK'

const ThemeContext = React.createContext({ isDark: null, toggleTheme: () => null })

const theme = {
  ...light,
  colors: {
    failure: '#ED4B9E',
    primary: '#315ddc',
    primaryBright: '#53DEE9',
    primaryDark: '#0098A1',
    secondary: '#7645D9',
    success: '#31D0AA',
    warning: '#FFB237',
    background: '#FAF9FA',
    backgroundDisabled: '#E9EAEB',
    backgroundAlt: '#FFFFFF',
    cardBorder: '#E7E3EB',
    contrast: '#191326',
    dropdown: '#F6F6F6',
    dropdownDeep: '#EEEEEE',
    invertedContrast: '#FFFFFF',
    input: '#eeeaf4',
    inputSecondary: '#d7caec',
    tertiary: '#EFF4F5',
    text: '#280D5F',
    textDisabled: '#BDC2C4',
    textSubtle: '#7A6EAA',
    disabled: '#E9EAEB',
    gradients: {
      bubblegum: 'linear-gradient(139.73deg, #E5FDFF 0%, #F3EFFF 100%)',
      inverseBubblegum: 'linear-gradient(139.73deg, #F3EFFF 0%, #E5FDFF 100%)',
      cardHeader: 'linear-gradient(111.68deg, #F2ECF2 0%, #E8F2F6 100%)',
      blue: 'linear-gradient(180deg, #A7E8F1 0%, #94E1F2 100%)',
      violet: 'linear-gradient(180deg, #E2C9FB 0%, #CDB8FA 100%)',
      violetAlt: 'linear-gradient(180deg, #CBD7EF 0%, #9A9FD0 100%)',
      gold: 'linear-gradient(180deg, #FFD800 0%, #FDAB32 100%)',
    },
    binance: '#F0B90B',
    overlay: '#452a7a',
  },
}

const ThemeContextProvider = ({ children }) => {
  return <SCThemeProvider theme={theme}>{children}</SCThemeProvider>
}

export { ThemeContext, ThemeContextProvider }
