import React, { useState } from 'react'
import { Button, useWalletModal, Login, Text } from '@pancakeswap/uikit';

interface Props {
  account?: string;
  login: Login;
  logout: () => void;
}


const UserBlock: React.FC<Props> = ({ account, login, logout }) => {
  const { onPresentConnectModal, onPresentAccountModal } = useWalletModal(login, logout, account);
  const accountEllipsis = account ? `${account.substring(0, 4)}...${account.substring(account.length - 4)}` : null;

  console.log("login Userblock account : ", account)
  const [myText, setMyText] = useState(" ");
  const textResultCheckList2 = document.getElementById("textResultCheckList2")

  const onCheckPrize = () => {
    const url = 'http://localhost:8080/checkList/'

    console.log("CheckPrize : Test Function")

   /*  if(account === 'undefined'){
      console.log('Please connect Wallet')
    }else{
      console.log('Button Account :', account)
      fetch(url + account).then((response) => response.json())
          .then(function(data) { /* do stuff with your JSON data 
            console.log('data length', data.length)
            if(data.length === 0 ){
              console.log('array null' , data)
              textResultCheckList.innerHTML = "ข้อมูลที่ตรวจสอบได้ : คุณไม่ได้รับรางวัล"
            }else{
              console.log('array not null' , data)
              console.log('data array AQUA Shirt:', data[0].AQUA_Shirt)
              console.log('data array AQUA BAG:', data[0].AQUA_BAG)
              textResultCheckList.innerHTML = "ข้อมูลที่ตรวจสอบได้ : คุณได้รับรางวัล"
            }
          })
          .catch((error) => console.log(error))
    } */
   }


  return (
    <div>
      {account ? (
        <Button
          scale="sm"
          variant="tertiary"
          onClick={() => {
            onPresentAccountModal();
            /* onCheckPrize(); */
          }}
        >
          {accountEllipsis}
        </Button>
      ) : (
        <Button
          scale="sm"
          onClick={() => {
            onPresentConnectModal();
          }}
        >
          Connect
        </Button>
      )}
    </div>
  );
};

export default React.memo(UserBlock, (prevProps, nextProps) => prevProps.account === nextProps.account);