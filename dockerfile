# First Stage ...
FROM node:15-alpine as builder
RUN mkdir -p /usr/src/app
COPY . /usr/src/app
WORKDIR /usr/src/app
COPY package.json .
RUN npm install 
# RUN npm run build
# Second Stage ...
FROM nginx:1.14.2-alpine
COPY --from=builder /usr/src/app/build /usr/share/nginx/html
WORKDIR /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]